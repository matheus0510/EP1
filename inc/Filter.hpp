#ifndef FILTER_HPP
#define FILTER_HPP
#include <iostream>
#include <fstream>

using namespace std;

class Filter
{
private:
        int div;
        int size;
	int *matrix;
	
public:
	Filter();
	Filter(int *matrix, int div, int size);
	
	int * getMatrix();
	int getSize();
	int getDiv();
	
	void setMatrix(int *matrix);
        void setSize(int size);
	void setDiv(int div);
	
	char * ApplyFilter(char *pixels, int height, int width, int *max);
};

#endif
