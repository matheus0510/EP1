#ifndef FILEREAD_H
#define FILEREAD_H
#include "Data.hpp"
#include <fstream>

using namespace std;

class FileRead : public Data
{

private:
	ifstream fileIn;

public:
	FileRead();
	FileRead(ifstream fileIn);

	ifstream getFileIn();
	
	void setFileIn(ifstream fileIn);

	void ReadFile(string name);
};

#endif
