#ifndef DATA_H
#define DATA_H
#include <string>
#include <fstream>

using namespace std;

class Data
{
private:
	string comment, P5;
	char *pixels;	
	int height, width, max;
		
public:
	Data();
	Data(string P5, string comment, int height, int width,  char *pixels, int max);
		
		string getP5();
		string getComment();
		int getHeight();
		int getWidth();
		int getMax();
		char * getPixels();
		
		void setP5(string P5);
		void setComment(string comment);
		void setHeight(int height);
		void setWidth(int width);
		void setPixels(char *pixels);
                void setMax(int max);

};

#endif
