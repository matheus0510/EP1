#ifndef FILEWRITE_H
#define FILEWRITE_H
#include "Data.hpp"
#include <fstream>

using namespace std;

class FileOutPGM : public Data
{

private:
	ofstream fileOut;

public:
	FileOutPGM();
	FileOutPGM(ifstream fileOut);

	ifstream * getFileOut();

	void setFileOut(ifstream fileOut);
	
	void WriteFile(string name, Data data);
};

#endif
