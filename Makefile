#Arquivo final executavel
BINFOLDER := bin/
#.hpp
INCFOLDER := inc/
#.cpp
SRCFOLDER := src/
#.o
OBJFOLDER := obj/

CC := g++

CFLAGS := -Wall -ansi
#ou:
#CFLAGS := -Wall -ansi -I./inc

#quando chamar "SRCFILES" tudo que for .c vai ser colocado no lugar.
SRCFILES := $(wildcard src/*.cpp)

#Pega todos os arquivos .c da SRCFILES e copia para a pasta:
#obj com o formato .o
all:	$(SRCFILES:src/%.cpp=obj/%.o)
	$(CC) $(CFLAGS) obj/*.o -o bin/finalBinary

#vai compilar o programa classe por classe.
obj/%.o:src/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@ -I./inc

#$< Pega o nome do primeiro arquivo.
#$@ Vai mandar os arquivos gerados para o alvo (/obj).
#-I ./inc vai procurar na pasta inc. (procurar os headers.

run:	bin/finalBinary
	bin/finalBinary

.PHONY: clean
clean:
	rm -rf obj/*
	rm -rf bin/*
