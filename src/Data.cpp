#include "Data.hpp"

using namespace std;

Data::Data()
{
	setP5("P5");
	setComment("comment");
	setWidth(2);
	setHeight(2);
	setMax(255);
	setPixels(NULL);
}

Data::Data(string P5, string comment, int width, int height, int max, char *pixels)
{
        setP5(P5);
        setComment(comment);
        setWidth(width);
        setHeight(height);
        setMax(max);
        setPixels(pixels);
}

string Data::getP5()
{
	return P5;
}

string Data::getComment()
{
	return comment;
}

int Data::getWidth()
{
	return width;
}

int Data::getHeight()
{
	return height;
}

int Data::getMax()
{
	return max;
}

char * getPixels()
{
	return pixels;
}

void Data::setP5(string P5)
{
	this->P5=P5;
}

void Data::setComment(string comment)
{
	this->comment = comment;
}

void Data::setWidth(int width)
{
	this->width = width;
}

void Data::setHeight(int height)
{
	this->height = height;
}

void Data::setMax(int max)
{
	this->max = max;
}

void Data::setPixels(char *pixels)
{
	this->pixels = pixels;
}
