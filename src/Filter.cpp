#include "filter.hpp"

using namespace std;

Filter::Filter(){
	setMatrix(NULL);
	setDiv(1);
	setSize(3);
}

Filtro::Filtro(int *matrix, int div, int size){
	setMatrix(matriz);
	setDiv(div);
	setSize(size);
}

int * Filter::getMatrix(){
	return matrix;
}

int Filter::getDiv(){
	return div;
}

int Filter::getSize(){
	return size;
}

void Filter::setMatrix(int *matrix){
	this->matrix = matrix;
}

void Filter::setDiv(int div){
	this->div = div;
}
void Filter::setSize(int size){
	this->size = size;
}

char * Filter::ApplyFilter(char *pixels, int width, int height, int *max){
	int value, i, j;
	char *new_pixels = new char[width*height];

	for(i=0; i<size; i++){
		for(j=0; j<width; j++){
			new_pixels[i+j*width] = pixels[i+j*width];
			new_pixels[(i+j*width)+width-1] = pixels[(i+j*width)+width-1];
		}
	}
	
	for(i=0; i<size; i++){
		for(j=0; j<height; j++){
			new_pixels[i*height+j] = pixels[i*height+j];
			new_pixels[(i*height+j)+height-1] = pixels[(i*height+j)+height-1];
		}
	}

	for(i=size/2; i<width-size/2; i++){
		for(j=size/2; j<height-size/2; j++){
			value = 0;
			for(int x=-(size/2); x<=size/2; x++){
				for(int y=-(size/2); y<=size/2;y++){
					value += matrix[(x+1)+size*(y+1)]*(unsigned char)pixels[(i+x)+(y+j)*width];
				}
			}
				value /= div;
				
				value = value < 0 ? 0 : value;
				value = value > 255 ? 255 : value;
				
				if(value > *max)
					*max = value;
				
				new_pixels[i+j*width] = value;
		}
	}
	
	return new_pixels;
}
