#include "FileRead.hpp"

using namespace std;

FileRead::FileRead()
{
	setFileIn(NULL);
}

FileRead::FileRead(ifstream fileIn)
{
	setFileIn(fileIn);
}

ifstream FileRead::getFileIn()
{
	return fileIn;
}

void FileRead::ReadFile(string name)
{
	fileIn.open(name);
	
	getline(file, P5);
	getline(file, comment);
	fileIn >> width >> height >> max;
	fileIn.get();

	pixels = new char[width*height];
	
	for(int i = 0; i < width; i++)
	{
		for(int j = 0; j < height; j++)
		{
			fileIn.get(pixels[i*width+j]);
		}
	}

	fileIn.close();
}
